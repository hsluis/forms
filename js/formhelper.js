var formhelper = {

    /**
     * Convert the Vue data to a FormData object
     *
     * @param data
     * @returns {*}
     */
    convertToFormData: function(data){
        var Data = new FormData();
        for (var key in data) {

            var element = data[key];

            // If it's a file object
            if(key.slice(-5) === "_data"){
                if(element.lastModified !== undefined) {

                    // Pass the base64 data and add the file name
                    Data.append(key, element, element.name);
                }
            }
            else {

                // Add an object
                if(typeof(element) === "object"){
                    Data.append(key, JSON.stringify(element));
                }

                // Add an array
                else if(typeof(element) === "array"){
                    Data.append(key, JSON.stringify(element));
                }

                // Add a regular string object
                else {
                    Data.append(key, element);
                }
            }
        }
        return Data;
    },

    /**
     * Return the value of an URL parameter
     *
     * @param param
     */
    getUrlParameter: function(param){
        var url_string = window.location.href;
        var url = new URL(url_string);
        var c = url.searchParams.get(param);
        return c;
    }
};

/**
 * Number.prototype.format(n, x, s, c)
 *
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};