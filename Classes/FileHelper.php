<?php
namespace Forms\Classes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileHelper {

    private $filename;

    public function storeFile(Request $request, $key, $path){
        if($request->has($key.'_data')){
            $file = Storage::disk('public')->putFileAs($path, $request->file($key.'_data'), str_slug(pathinfo($request->file($key.'_data')->getClientOriginalName(), PATHINFO_FILENAME)).'.'.$request->file($key.'_data')->getClientOriginalExtension());
            $this->filename = pathinfo($file, PATHINFO_BASENAME);
            return true;
        }
        else {
            return false;
        }
    }

    public function getFilename(){
        return $this->filename;
    }
}