<div class="f1-steps">
    <div class="f1-progress">
        <div class="f1-progress-line" data-now-value="{{ number_format(100 / (count($steps)*2), 2, '.', '') }}" data-number-of-steps="{{ count($steps) }}" style="width: {{ number_format(100 / (count($steps)*2), 2, '.', '') }}%;"></div>
    </div>
    @foreach($steps as $key => $step)
        <div class="f1-step @if($key == 0)active @endif">
            <div class="f1-step-icon">{!! $step['icon'] !!}</div>
            <p>{{ $step['text'] }}</p>
        </div>
    @endforeach
</div>