<?php
    if(empty($id)){
        $id = uniqid();
    }
    if(empty($vueNamespace)){
        $vueNamespace = 'app';
    }
    if(empty($type)){
        $type = 'regular';
    }
?>

<div id="form-group-{{ $id }}" class="form-group" :class="@if(!empty($errorKey)){ 'has-error' : errors.{{ $errorKey }} !== undefined}@endif">
    @if(!empty($label))<label for="{{ $id }}">{{ $label }}</label>@endif
    <select class="form-control @if($type == 'select2')select2 @endif" v-model="{{ $vueKey }}" id="{{ $id }}">
        @foreach($options as $option)
            <option value="{{ $option['value'] }}">{{ $option['text'] }}</option>
        @endforeach
    </select>
    @include('forms.error-block', [
        'errorKey' => !empty($errorKey) ? $errorKey: null
    ])
</div>

@section('script')
    @parent
    @if($type === 'select2')
        <script>
            $(document).ready(function(){
                $('#form-group-{{ $id }}').find(".select2").select2();
                $('#form-group-{{ $id }}').find(".select2").on('select2:select', function (e) {
                    window.{{ $vueNamespace }}.$data.{{ $vueKey }} = e.params.data.id;
                });
            });
        </script>
    @endif
@endsection