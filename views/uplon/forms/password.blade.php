<?php
    if(empty($id)){
        $id = uniqid();
    }
    if(empty($vueNamespace)){
        $vueNamespace = 'app';
    }
?>

<div class="form-group" :class="{ 'has-error' : errors.{{ $errorKey }} !== undefined}">
    @if(!empty($label))<label for="{{ $id }}">{{ $label }}</label>@endif
    <input v-model="{{ $vueKey }}" type="password" class="form-control" id="{{ $id }}">
    @include('forms.error-block', [
        'errorKey' => !empty($errorKey) ? $errorKey: null
    ])
</div>