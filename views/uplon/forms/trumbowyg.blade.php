{{--<script src="node_modules/trumbowyg/dist/trumbowyg.min.js"></script>--}}
{{--<link rel="stylesheet" href="node_modules/trumbowyg/dist/ui/trumbowyg.min.css">--}}

<?php
    if(empty($id)){
        $id = uniqid();
    }
    if(empty($vueNamespace)){
        $vueNamespace = 'app';
    }
?>

<div class="form-group" :class="{ 'has-error' : errors.{{ $errorKey }} !== undefined}">
    @if(!empty($label))<label for="{{ $id }}">{{ $label }}</label>@endif
    <div id="{{ $id }}"></div>
    @include('forms.error-block', [
        'errorKey' => !empty($errorKey) ? $errorKey: null
    ])
</div>

@section('script')
    @parent
    <script>
        $(document).ready(function(){

            // Set the div contents from Vue
            $('#{{ $id }}').html(window.{{ $vueNamespace }}.$data.{{ $vueKey }});

            // Initialize trumbowyg
            $('#{{ $id }}').trumbowyg()
                .on('tbwchange', function(e){

                    // Set the value to Vue.js
                    window.{{ $vueNamespace }}.$data.{{ $vueKey }} = e.target.innerHTML;
                });
        })
    </script>
@endsection