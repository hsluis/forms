{{--
    Bootstrap 3 only
--}}

<?php
    if(empty($id)){
        $id = uniqid();
    }
    if(empty($vueNamespace)){
        $vueNamespace = 'app';
    }
?>

<div class="form-group" :class="@if(!empty($errorKey)){ 'has-error' : errors.{{ $errorKey }} !== undefined}@endif">
    @if(!empty($label))<label for="{{ $id }}">{{ $label }}</label>@endif
    <input type="text" class="form-control" id="{{ $id }}">
    @include('forms.error-block', [
        'errorKey' => !empty($errorKey) ? $errorKey: null
    ])
</div>

@section('script')
    @parent
    <script>
        $(document).ready(function () {

            // Set the initial value
            var moment = window.moment(window.{{ $vueNamespace }}.$data.{{ $vueKey }});
            $('#{{ $id }}').val(moment.format('DD-MM-YYYY HH:mm'));

            // Initialize the datetime picker
            $('#{{ $id }}').datetimepicker({
                format: 'DD-MM-Y HH:mm'
            });

            // Update the vue value on change
            $('#{{ $id }}').on('dp.change', function(moment){
                window.{{ $vueNamespace }}.$data.{{ $vueKey }} = moment.date.format('Y-MM-DD HH:mm:00');
            });
        });
    </script>
@endsection
