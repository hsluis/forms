<?php
    if(empty($id)){
        $id = uniqid();
    }
    if(empty($vueNamespace)){
        $vueNamespace = 'app';
    }
?>

<div class="radio radio-primary">
    <input id="{{ $id }}" type="radio" v-model="{{ $vueKey }}" value="{{ $value }}">
    <label for="{{ $id }}">
        {{ $label }}
    </label>
</div>