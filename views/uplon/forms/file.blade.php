<?php
    if(empty($id)){
        $id = uniqid();
    }
    if(empty($vueNamespace)){
        $vueNamespace = 'app';
    }
?>

<div class="form-group" :class="{ 'has-error' : errors.{{ $errorKey }} !== undefined}">
    @if(!empty($label))<label for="{{ $id }}">{{ $label }}</label>@endif
    <div id="{{ $id }}_container">
        <div class="file">
            <input type="file" id="{{ $id }}">
        </div>
    </div>
    @include('forms.error-block', [
        'errorKey' => !empty($errorKey) ? $errorKey.'_data' : null
    ])
</div>

@section('script')
    @parent
    <script>
        var file_{{ $id }} = {
            init: function(options){

                // Create an empty options object
                if(options === undefined){
                    options = {};
                }

                $(document).ready(function () {

                    // Get the preview (lazy loaded)
                    var preview = '';
                    if(options.preview !== undefined){
                        preview = options.preview;
                    }

                    // Get the preview (non-lazy loaded)
                    else if(window.{{ $vueNamespace }}.$data.{{ $vueKey }} !== "") {
                        preview = '{{ $path or '' }}/' + window.{{ $vueNamespace }}.$data.{{ $vueKey }};
                    }

                    // Change the preview image
                    var dropifyEvent = $('#{{ $id }}').data('dropify');
                    if(dropifyEvent !== undefined){
                        var dropifyWrapper = $('#{{ $id }}').closest('.dropify-wrapper');
                        var previewWrapper = dropifyWrapper.find('.dropify-preview');
                        if(preview === ''){
                            previewWrapper.hide();
                        }
                        else {
                            dropifyWrapper.find('.dropify-render img').attr('src', preview);
                            previewWrapper.show();
                        }
                    }

                    // Initialize dropify
                    else {
                        $('#{{ $id }}').dropify({
                            defaultFile: preview
                        });
                    }

                    // Pass the values to Vue after selecting an image (empty the filename, because it gets slugified in Laravel)
                    $('#{{ $id }}').on('change', function(e){
                        window.{{ $vueNamespace }}.$data.{{ $vueKey }} = '';
                        window.{{ $vueNamespace }}.$data.{{ $vueKey }}_data = e.target.files[0];
                    });

                    // Empty all after clearing the image
                    $('#{{ $id }}').on('dropify.afterClear', function(event, element){
                        window.{{ $vueNamespace }}.$data.{{ $vueKey }}_data = {};
                        window.{{ $vueNamespace }}.$data.{{ $vueKey }} = '';
                    });
                });
            }
        };

        // Initialize dropify if not lazy
        @if(empty($lazy) || $lazy === false)
            file_{{ $id }}.init();
        @endif
    </script>
@endsection