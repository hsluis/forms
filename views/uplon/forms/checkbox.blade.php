<?php
    if(empty($id)){
        $id = uniqid();
    }
    if(empty($vueNamespace)){
        $vueNamespace = 'app';
    }
?>
<div class="checkbox checkbox-primary">
    <input id="{{ $id }}" type="checkbox" v-model="{{ $vueKey }}">
    <label for="{{ $id }}">
        {{ $label }}
    </label>
</div>