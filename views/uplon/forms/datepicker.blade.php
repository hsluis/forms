<?php
    if(empty($id)){
        $id = uniqid();
    }
    if(empty($vueNamespace)){
        $vueNamespace = 'app';
    }
?>

<div class="form-group" :class="@if(!empty($errorKey)){ 'has-error' : errors.{{ $errorKey }} !== undefined}@endif">
    @if(!empty($label))<label for="{{ $id }}">{{ $label }}</label>@endif
    <div class="input-group">
        <input type="text" class="form-control" placeholder="DD-MM-JJJJ" id="{{ $id }}">
        <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
    </div>
    @include('forms.error-block', [
        'errorKey' => !empty($errorKey) ? $errorKey: null
    ])
</div>

@section('script')
    @parent
    <script>
        var forms_datepicker_{{ $id }} = {
            init: function () {
                $(document).ready(function () {

                    // Set the initial value
                    if(window.{{ $vueNamespace }}.$data.{{ $vueKey }} !== ''){
                        var moment = window.moment(window.{{ $vueNamespace }}.$data.{{ $vueKey }});
                        $('#{{ $id }}').val(moment.format('DD-MM-YYYY'));
                    }

                    // Initialize the datetime picker
                    $('#{{ $id }}').datepicker({
                        autoclose: true,
                        todayHighlight: true,
                        format: 'dd-mm-yyyy',
                        startView: '{{ $startView or 'days' }}'
                    }).on('changeDate', function(e){
                        window.{{ $vueNamespace }}.$data.{{ $vueKey }} = e.format('yyyy-mm-dd');
                    }).on('clearDate', function(e){
                        window.{{ $vueNamespace }}.$data.{{ $vueKey }} = '';
                    });
                });
            }
        };

        forms_datepicker_{{ $id }}.init();
    </script>
@endsection
