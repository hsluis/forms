<?php
    if(empty($id)){
        $id = uniqid();
    }
    if(empty($vueNamespace)){
        $vueNamespace = 'app';
    }
?>
<div class="form-group" :class="@if(!empty($errorKey)){ 'has-error' : errors.{{ $errorKey }} !== undefined}@endif">
    <div>
        @if(!empty($label))<label for="{{ $id }}">{{ $label }}</label>@endif
        <input type="text" id="{{ $id }}" class="form-control" style="z-index: 2; background: transparent;">
        <input type="hidden" id="{{ $id }}" v-model="{{ $vueKey }}">
        @include('forms.error-block', [
            'errorKey' => !empty($errorKey) ? $errorKey: null
        ])
    </div>
</div>

@section('script')
    @parent
    <script type="text/javascript">
        $(document).ready(function(){
            var autocomplete = $('#{{ $id }}').autocomplete({
                serviceUrl: '{{ $serviceUrl }}',
                dataType: 'json',
                onSearchStart: function(){
                    window.{{ $vueNamespace }}.$data.{{ $vueKey }} = '';
                },
                onSearchComplete: function(query, suggestions){
                    if(suggestions.length === 0){
                        $('#{{ $id }}').val('');
                    }
                },
                onSelect: function (suggestion) {
                    $('#{{ $id }}').val(suggestion.value);
                    window.{{ $vueNamespace }}.$data.{{ $vueKey }} = suggestion.data;
                },
                onHide: function(){
                    if(window.{{ $vueNamespace }}.$data.{{ $vueKey }} === ''){
                        $('#{{ $id }}').val('');
                    }
                }
            });

            axios.request({
                url: '{{ $serviceUrl }}',
                responseType: 'json'
            })
            .then(function (response) {
                for(var i=0; i < response.data.suggestions.length; i++){
                    if(response.data.suggestions[i].data == window.{{ $vueNamespace }}.$data.{{ $vueKey }}){
                        $('#{{ $id }}[type="text"]').val(response.data.suggestions[i].value);
                    }
                }
            })
            .catch(function (error) {

            })
            .then(function () {

            });
        });
    </script>
@endsection
