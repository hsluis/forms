<?php
    if(empty($id)){
        $id = uniqid();
    }
    if(empty($vueNamespace)){
        $vueNamespace = 'app';
    }
?>

<div class="form-group" :class="{ 'has-error' : errors.{{ $errorKey }} !== undefined}">
    @if(!empty($label))<label for="{{ $id }}">{{ $label }}</label>@endif
    <textarea v-model="{{ $vueKey }}" class="form-control" rows="5" id="{{ $id }}"></textarea>
    @include('forms.error-block', [
        'errorKey' => !empty($errorKey) ? $errorKey: null
    ])
</div>