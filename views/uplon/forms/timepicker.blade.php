<?php
    if(empty($id)){
        $id = uniqid();
    }
    if(empty($vueNamespace)){
        $vueNamespace = 'app';
    }
?>

<div class="form-group" :class="{ 'has-error' : errors.{{ $errorKey }} !== undefined}">
    @if(!empty($label))<label for="{{ $id }}">{{ $label }}</label>@endif
    <div class="input-group timepicker">
        <input v-model="{{ $vueKey }}" id="{{ $id }}" type="text" class="form-control">
        <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
    </div>
    @include('forms.error-block', [
        'errorKey' => !empty($errorKey) ? $errorKey: null
    ])
</div>

@section('script')
    @parent
    <script>
        var forms_timepicker_{{ $id }} = {
            init: function(){
                $(document).ready(function() {
                    $('#{{ $id }}').timepicker({
                        showMeridian: false,
                        snapToStep: true,
                        icons: {
                            up: 'zmdi zmdi-chevron-up',
                            down: 'zmdi zmdi-chevron-down'
                        }
                    }).on('changeTime.timepicker', function(e){
                        var hours=e.time.hours, //Returns a string
                            min=e.time.minutes,
                            meridian=e.time.meridian;

                        if(hours.toString().length==1){
                            hours = '0'+hours;
                        }
                        else if(hours.toString().length==0){
                            hours = '00';
                        }

                        if(min.toString().length==1){
                            min = '0'+min;
                        }
                        else if(min.toString().length==0){
                            min = '00';
                        }
                        window.{{ $vueNamespace }}.$data.{{ $vueKey }} = hours+':'+min;
                    });
                });
            },
            reInit: function(){
                $('#{{ $id }}').timepicker('setTime', window.{{ $vueNamespace }}.$data.{{ $vueKey }});

            }
        };

        forms_timepicker_{{ $id }}.init();
    </script>
@endsection