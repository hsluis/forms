<div class="form-group" :class="{ 'has-error' : errors.{{ $dbField }} !== undefined}">
    @if(!empty($label))<label for="{{ $dbField }}">{{ $label }}</label>@endif
    <div class="input-group timepicker">
        <input v-model="{{ $vueKey }}" id="{{ $dbField }}" type="text" class="form-control">
        <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
    </div>
    <div class="help-block with-errors" v-if="errors.{{ $dbField }} !== undefined">
        <ul class="list-unstyled">
            <li v-for="error in errors.{{ $dbField }}">@{{ error }}</li>
        </ul>
    </div>
</div>

@section('script')
    @parent
    <script>
        $(document).ready(function() {
            $('#{{ $dbField }}').timepicker({
                showMeridian: false,
                icons: {
                    up: 'zmdi zmdi-chevron-up',
                    down: 'zmdi zmdi-chevron-down'
                }
            }).on('changeTime.timepicker', function(e){
                window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }} = e.time.hours+':'+e.time.minutes;
            });
        });
    </script>
@endsection