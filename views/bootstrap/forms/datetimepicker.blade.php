{{--
    Bootstrap 3 only
--}}

<div class="form-group" :class="{ 'has-error' : errors.{{ $dbField }} !== undefined}">
    @if(!empty($label))<label for="{{ $dbField }}">{{ $label }}</label>@endif
    <input type="text" class="form-control" id="{{ $dbField }}">
    <div class="help-block with-errors" v-if="errors.{{ $dbField }} !== undefined">
        <ul class="list-unstyled">
            <li v-for="error in errors.{{ $dbField }}">@{{ error }}</li>
        </ul>
    </div>
</div>

@section('script')
    @parent
    <script>
        $(document).ready(function () {

            // Set the initial value
            var moment = window.moment(window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }});
            $('#{{ $dbField }}').val(moment.format('DD-MM-YYYY HH:mm'));

            // Initialize the datetime picker
            $('#{{ $dbField }}').datetimepicker({
                format: 'DD-MM-Y HH:mm'
            });

            // Update the vue value on change
            $('#{{ $dbField }}').on('dp.change', function(moment){
                window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }} = moment.date.format('Y-MM-DD HH:mm:00');
            });
        });
    </script>
@endsection
