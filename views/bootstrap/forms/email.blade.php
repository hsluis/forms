{{--
    @include('forms.text', [
        'errorKey' => '',
        'vueKey' => '',
        'label' => ''
    ])
--}}
<div class="form-group" :class="{ 'has-error' : errors.{{ $dbField }} !== undefined}">
    @if(!empty($label))<label for="{{ $dbField }}">{{ $label }}</label>@endif
    <input v-model="{{ $vueKey }}" type="email" class="form-control" id="{{ $dbField }}">
    @if(!empty($note))<small>{{ $note }}</small>@endif
    <div class="help-block with-errors" v-if="errors.{{ $dbField }} !== undefined">
        <ul class="list-unstyled">
            <li v-for="error in errors.{{ $dbField }}">@{{ error }}</li>
        </ul>
    </div>
</div>