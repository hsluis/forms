<?php
    $randId = uniqid();
?>
<div>
    <input id="checkbox_{{ $randId }}" type="checkbox" v-model="{{ $vueKey }}">
    <label for="checkbox_{{ $randId }}">
        {{ $label }}
    </label>
</div>