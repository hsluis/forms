<div class="form-group" :class="{ 'has-error' : errors.{{ $dbField }} !== undefined}">
    @if(!empty($label))<label for="{{ $dbField }}_autocomplete">{{ $label }}</label>@endif
    <input type="text" id="{{ $dbField }}_autocomplete" class="form-control" style="z-index: 2; background: transparent;">
    <input type="hidden" id="{{ $dbField }}" v-model="{{ $vueKey }}">
    <div class="help-block with-errors" v-if="errors.{{ $dbField }} !== undefined">
        <ul class="list-unstyled">
            <li v-for="error in errors.{{ $dbField }}">@{{ error }}</li>
        </ul>
    </div>
</div>

@section('script')
    @parent
    <script type="text/javascript">
        $(document).ready(function(){
            $('#{{ $dbField }}_autocomplete').autocomplete({
                serviceUrl: '{{ $serviceUrl }}',
                dataType: 'json',
                onSearchStart: function(){
                    window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }} = '';
                },
                onSearchComplete: function(query, suggestions){
                    if(suggestions.length === 0){
                        $('#{{ $dbField }}_autocomplete').val('');
                    }
                },
                onSelect: function (suggestion) {
                    $('#{{ $dbField }}_autocomplete').val(suggestion.value);
                    window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }} = suggestion.data;
                },
                onHide: function(){
                    if(window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }} === ''){
                        $('#{{ $dbField }}_autocomplete').val('');
                    }
                }
            });
        });
    </script>
@endsection
