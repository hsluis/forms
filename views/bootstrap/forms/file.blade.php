<div class="form-group" :class="{ 'has-error' : errors.{{ $dbField }} !== undefined}">
    @if(!empty($label))<label for="{{ $dbField }}">{{ $label }}</label>@endif
    <input type="file" id="{{ $dbField }}">
    <div class="help-block with-errors" v-if="errors.{{ $dbField }}_data !== undefined">
        <ul class="list-unstyled">
            <li v-for="error in errors.{{ $dbField }}_data">@{{ error }}</li>
        </ul>
    </div>
</div>

@section('script')
    @parent
    <script>
        $(document).ready(function () {

            // Set the initial image
            if(window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }} != ""){
                $('#{{ $dbField }}').attr('data-default-file', '{{ $path }}/'+window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }});
            }

            // Initiate dropify
            $('#{{ $dbField }}').dropify();

            // Pass the values to Vue after selecting an image (empty the filename, because it gets slugified in Laravel)
            $('#{{ $dbField }}').on('change', function(e){
                window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }} = '';
                window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }}_data = e.target.files[0];
            });

            // Empty all after clearing the image
            $('#{{ $dbField }}').on('dropify.afterClear', function(event, element){
                window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }}_data = {};
                window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }} = '';
            });
        });
    </script>
@endsection