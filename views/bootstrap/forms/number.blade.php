<div class="form-group" :class="{ 'has-error' : errors.{{ $dbField }} !== undefined}">
    @if(!empty($label))<label for="{{ $dbField }}">{{ $label }}</label>@endif
    <input v-model="{{ $vueKey }}" type="number" class="form-control" id="{{ $dbField }}">
    <div class="help-block with-errors" v-if="errors.{{ $dbField }} !== undefined">
        <ul class="list-unstyled">
            <li v-for="error in errors.{{ $dbField }}">@{{ error }}</li>
        </ul>
    </div>
</div>