{{--<script src="node_modules/trumbowyg/dist/trumbowyg.min.js"></script>--}}
{{--<link rel="stylesheet" href="node_modules/trumbowyg/dist/ui/trumbowyg.min.css">--}}
<div class="form-group" :class="{ 'has-error' : errors.{{ $dbField }} !== undefined}">
    @if(!empty($label))<label for="{{ $dbField }}">{{ $label }}</label>@endif
    <div id="{{ $dbField }}"></div>
    <div class="help-block with-errors" v-if="errors.{{ $dbField }} !== undefined">
        <ul class="list-unstyled">
            <li v-for="error in errors.{{ $dbField }}">@{{ error }}</li>
        </ul>
    </div>
</div>

@section('script')
    @parent
    <script>
        $(document).ready(function(){

            // Set the div contents from Vue
            var output = $("<div />").html(window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }}).text();
            $('#{{ $dbField }}').html(output);

            // Initialize trumbowyg
            $('#{{ $dbField }}').trumbowyg()
                .on('tbwchange', function(e){

                    // Set the value to Vue.js
                    window.{{ $vueNamespace or 'app' }}.$data.{{ $vueKey }} = e.target.innerHTML;
                });
        })
    </script>
@endsection