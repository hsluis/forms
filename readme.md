Installation
---

1. Extract this archive into /vendor/forms
2. Add the following line to the psr-4 section of composer.json: `"Forms\\": "vendor/forms/"`
3. Run `composer dump-autoload`
4. Add this line to webpack.js: `.copy('vendor/forms/js', 'public/vendor/forms')`
5. Add this line to .gitignore: `/public/vendor/forms`
6. Run `npm install` and `npm run dev`
7. Add the following to your layout.blade.php `<script src="/vendor/forms/formhelper.js"></script>`
8. Add this line to the paths array in config/view.php: `base_path('vendor/forms/views/bootstrap')`